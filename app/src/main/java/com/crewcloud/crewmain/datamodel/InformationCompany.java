package com.crewcloud.crewmain.datamodel;

public class InformationCompany {
    public double Latitude;
    public double Longitude;
    public String Description;
    public int LocationNo;
    public int IsWorking;
    public int ErrorRange;
}
